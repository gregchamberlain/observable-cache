import { Observable, Observer } from 'rxjs';
import ObservableCache from '../src/observable_cache';

const TEST_KEY = 'test-key';
const TEST_VALUE = 'test-value';

describe(ObservableCache, () => {
  let cache: ObservableCache<string, string>;
  let filledCache: ObservableCache<string, string>;
  let watcher: Observer<string>;
  beforeEach(() => {
    cache = new ObservableCache();
    const values = new Map();

    watcher = {
      next: jest.fn(),
      complete: jest.fn(),
      error: jest.fn(),
    };
    cache.watch(TEST_KEY).subscribe(watcher);

    values.set(TEST_KEY, TEST_VALUE);
    filledCache = new ObservableCache(values);
  });
  describe('has', () => {
    it('Returns false if a value is not in the cache', () => {
      expect(cache.has(TEST_KEY)).toBe(false);
    });
    it('Returns true if a value is in the cache', () => {
      expect(filledCache.has(TEST_KEY)).toBe(true);
    });
  });
  describe('get', () => {
    it('Returns undefined if a value is not in the cache', () => {
      expect(cache.get(TEST_KEY)).toBeUndefined();
    });
    it('Returns a value if it is in the cache', () => {
      expect(filledCache.get(TEST_KEY)).toBe(TEST_VALUE);
    });
  });
  describe('set', () => {
    it('Sets a value in the cache', () => {
      cache.set(TEST_KEY, TEST_VALUE);

      expect(cache.get(TEST_KEY)).toBe(TEST_VALUE);
    });
    it("Invokes all observers's next callback with the new value", () => {
      cache.set(TEST_KEY, TEST_VALUE);

      expect(watcher.next).toHaveBeenCalledTimes(1);
      expect(watcher.next).toHaveBeenCalledWith(TEST_VALUE);
    });
  });
  describe('delete', () => {
    it('Deletes a value from the cache', () => {
      filledCache.delete(TEST_KEY);

      expect(filledCache.has(TEST_KEY)).toBe(false);
    });
    it("Invokes all observers' complete callback", () => {
      cache.delete(TEST_KEY);

      expect(watcher.complete).toHaveBeenCalledTimes(1);
    });
  });
  describe('watch', () => {
    it('Returns an observable', () => {
      const observable = cache.watch(TEST_KEY);

      expect(observable).toBeInstanceOf(Observable);
    });
  });
});
