# ObservableCache · [![Build](https://gitlab.com/gregchamberlain/observable-cache/badges/master/pipeline.svg)](https://gitlab.com/gregchamberlain/observable-cache/pipelines?scope=branches) [![NPM](https://img.shields.io/npm/v/@gregchamberlain/observable-cache.svg)](https://www.npmjs.com/package/@gregchamberlain/observable-cache) [![Minified Size](https://badgen.net/bundlephobia/min/@gregchamberlain/observable-cache)](https://bundlephobia.com/result?p=@gregchamberlain/observable-cache) [![Minified Gzip Size](https://badgen.net/bundlephobia/minzip/@gregchamberlain/observable-cache)](https://bundlephobia.com/result?p=@gregchamberlain/observable-cache)

A simple cache implementation that allows for subscribing to value changes.

# Installation

```
npm install @gregchamberlain/observable-cache
```

or

```
yarn install @gregchamberlain/observable-cache
```

# Usage

```ts
import ObservableCache from '@gregchamberlain/observable-cache';

const cache: ObservableCache<string, string> = new ObservableCache();

let observable = cache.watch('someKey');
let subscription = observable.subscribe({
  next: (value) => console.log('Next:', value),
  complete: () => console.log('Complete'),
});
// --- Console log ---

cache.set('someKey', 'first value');
// --- Console Log ---
// Next: first value

subscription.unsubscribe();

cache.set('someKey', 'second value');
// --- Console log ---

observable = cache.watch('someKey');
subscription = observable.subscribe({
  next: (value) => console.log('Next:', value),
  complete: () => console.log('Complete'),
});
// --- Console log ---
// Next: second value

cache.delete('someKey');
// --- Console Log ---
// Complete

cache.set('someKey', 'second value');
// --- Console Log ---
```

# API

```ts
import { Observable } from 'rxjs';

interface ObservableCache<K, V> {
  /**
   * Returns whether a value exists for the given key.
   */
  has(key: K): boolean;

  /**
   * Gets the value for the given key, or undefined if none exists.
   */
  get(key: K): V | undefined;

  /**
   * Sets the given value for the given key. This will invoke the 'next'
   * callback of all observers of that key.
   */
  set(key: K, value: V): this;

  /**
   * Deletes any value for the given key and returns whether a value existed
   * before deletion. This will invoke the 'complete' callback of all observers
   * of that key.
   */
  delete(key: K): boolean;

  /**
   * Returns an observer that watches the value of the given key. If a value
   * exists when the observer is subscribed to the 'next' callback will be
   * invoked immediately with the existing value.
   */
  watch(key: K): Observable<V>;
}
```
