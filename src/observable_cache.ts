import { Observable, Subject } from 'rxjs';

class ObservableCache<K, V> {
  private valueMap: Map<K, V>;
  private subjectMap: Map<K, Subject<V>> = new Map();

  constructor(valueMap: Map<K, V> = new Map()) {
    this.valueMap = valueMap;
  }

  has(key: K) {
    return this.valueMap.has(key);
  }

  get(key: K) {
    return this.valueMap.get(key);
  }

  set(key: K, value: V) {
    this.valueMap.set(key, value);
    this.getSubject(key).next(value);
    return this;
  }

  delete(key: K) {
    const deleted = this.valueMap.delete(key);
    this.getSubject(key).complete();
    return deleted;
  }

  watch(key: K): Observable<V> {
    return this.getSubject(key);
  }

  private getSubject(key: K): Subject<V> {
    let subject = this.subjectMap.get(key);
    if (!subject) {
      subject = new Subject();
      this.subjectMap.set(key, subject);
    }
    return subject;
  }
}

export default ObservableCache;
